<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Acme\DemoBundle\Entity\Article;
use Acme\DemoBundle\Form\ArticleType;

class WelcomeController extends Controller
{
    public function indexAction()
    {
        /*
         * The action's view can be rendered using render() method
         * or @Template annotation as demonstrated in DemoController.
         *
         */
        return $this->render('AcmeDemoBundle:Welcome:index.html.twig');
    }
    
    public function form1Action() {
      
      $post = Request::createFromGlobals();
      
      if ($post->request->has('submit')) {
        $name = $post->request->get('name');
      }
      else {
        $name = 'Not submitted yet';
      }
      
      return $this->render('AcmeDemoBundle:Welcome:form1.html.twig', array('name' => $name));
      
    }
    
    public function form2Action(Request $request) {
      
      $article = new Article();
      
      $form = $this->createForm(new ArticleType(), $article); 
      
      $form->handleRequest($request);
      
      if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $em->persist($article);
        $em->flush();
        
        $session = $this->getRequest()->getSession();
        $session->getFlashBag()->add('message', 'Article saved!');
        
        return $this->redirect($this->generateUrl('_form2saved'));   
      }
      
      return $this->render('AcmeDemoBundle:Welcome:form2.html.twig', array('form' => $form->createView()));
      
    }
      
    public function form2savedAction() {
      
      return $this->render('AcmeDemoBundle:Welcome:form2saved.html.twig');
     
    }
    
    
}
